import 'package:bullega_api/controller/award_history_controller.dart';
import 'package:bullega_api/controller/login_controller.dart';
import 'package:bullega_api/controller/news_controller.dart';
import 'package:bullega_api/controller/profile_controller.dart';
import 'package:bullega_api/controller/varify_controller.dart';

import 'bullega_api.dart';

class BullegaApiChannel extends ApplicationChannel {
  ManagedContext context;

  @override
  Future prepare() async {
    logger.onRecord.listen(
        (rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));

    final config = BullegaApiConfiguration(options.configurationFilePath);
    context = contextWithConnectionInfo(config.database);
  }

  @override
  Controller get entryPoint {
    final router = Router()
      ..route("/News/[:id]").link(() => NewsController(context))
      ..route("/award/[:id]").link(() => AwardHistoryController(context))
      ..route("/login").link(() => LoginController(context))
      ..route("/profile").link(() => ProfileController(context))
      ..route("/varify").link(() => VarifyController(context));

    return router;
  }

  /*
   * Helper methods
   */

  ManagedContext contextWithConnectionInfo(
      DatabaseConfiguration connectionInfo) {
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final psc = PostgreSQLPersistentStore(
        connectionInfo.username,
        connectionInfo.password,
        connectionInfo.host,
        connectionInfo.port,
        connectionInfo.databaseName);

    return ManagedContext(dataModel, psc);
  }
}

class BullegaApiConfiguration extends Configuration {
  BullegaApiConfiguration(String fileName) : super.fromFile(File(fileName));

  DatabaseConfiguration database;
}
