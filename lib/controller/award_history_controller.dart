import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/award.dart';

class AwardHistoryController extends ResourceController {
  ManagedContext context;
  AwardHistoryController(this.context);

  @Operation.get()
  Future<Response> getAllNews() async {
    final query = Query<Award>(context)
      ..sortBy((award) => award.date, QuerySortOrder.descending);

    final awardData = await query.fetch();

    return Response.ok(awardData);
  }

  @Operation.get("id")
  Future<Response> getAwardById(@Bind.path('id') int id) async {
    final query = Query<Award>(context)..where((d) => d.id).equalTo(id);
    final award = await query.fetchOne();
    return Response.ok(award);
  }
}
