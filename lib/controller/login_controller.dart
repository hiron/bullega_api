import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/login.dart';
import 'package:password/password.dart';
import 'package:mailer2/mailer.dart';

class LoginController extends ResourceController {
  LoginController(this.context);
  ManagedContext context;

  @Operation.post()
  Future<Response> checkLogin(@Bind.body() Login login) async {
    final query = Query<Login>(context)
      ..where((l) => l.email).equalTo(login['email'] as String)
      ..join(object: (l) => l.profile);

    final loginInfo = await query.fetchOne();

    if (Password.verify(
        login['password'] as String, loginInfo.hashedPassword)) {
      if (loginInfo.state == 'lock') {
        final String password = 'btyanlvrlzadgrkr';
        final String email = 'hcdas.09@gmail.com';

        final options = new GmailSmtpOptions()
          ..username = email
          ..password = password;

        final emailTransport = new SmtpTransport(options);

        var envelope = new Envelope()
          ..from = 'foo@bar.com'
          ..recipients.add(loginInfo.email)
          // ..bccRecipients.add('hidden@recipient.com')
          ..subject = 'Bullega Email Varification'
          // ..attachments.add(new Attachment(file: new File('path/to/file')))
          ..text =
              'Hi ${loginInfo.profile.firstname}, \n The Varifination code for the app is ${loginInfo.varifyCode}'
          ..html = '<h1>Test</h1><p>Hey!</p>';

        emailTransport
            .send(envelope)
            .then((envelope) => Response.ok("email sent!!"))
            .catchError((e) => print("Error message: $e"));
      }
      return Response.ok(loginInfo);
    }

    return Response.serverError();
  }
}
