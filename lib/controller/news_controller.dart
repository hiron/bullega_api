import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/news.dart';

class NewsController extends ResourceController {
  ManagedContext context;
  NewsController(this.context);

  @Operation.get()
  Future<Response> getAllNews(@Bind.query("search") String search ) async {
    final query = Query<News>(context)
      ..pageBy((news) => news.date, QuerySortOrder.descending)
      ..fetchLimit = 10;

    if(search.isNotEmpty){
      query.where((d)=> d.title).contains(search, caseSensitive: false);
    }

    final newsData = await query.fetch();

    return Response.ok(newsData);
  }

  @Operation.get("id")
  Future<Response> getNewsById(@Bind.path('id') int id) async {
    final query = Query<News>(context)..where((d) => d.id).equalTo(id);
    final news = await query.fetchOne();
    return Response.ok(news);
  }
}
