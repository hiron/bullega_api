import 'dart:math';

import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/login.dart';
import 'package:bullega_api/model/profile.dart';

class ProfileController extends ResourceController {
  ManagedContext context;

  ProfileController(this.context);

  @Operation.post()
  Future<Response> addProfile(@Bind.body() Profile profile) async {
    final countProfile = Query<Profile>(context)
      ..where((p) => p.login.email).equalTo(profile.email);

    if (await countProfile.reduce.count() == 0) {
      await context.transaction((transaction) async {
        final profilequery = Query<Profile>(transaction)..values = profile;
        final addProfile = await profilequery.insert();

        final int randomVarifyCode =
            [1, 2, 3, 4, 5].map((d) => Random().nextInt(9)).join("") as int;

        final loginQuery = Query<Login>(transaction)
          ..values.profile = addProfile
          ..values.varifyCode = randomVarifyCode
          ..values.email = profile['email'] as String
          ..values.password = profile['password'] as String;

        await loginQuery.insert();

        return Response.ok(addProfile);
      });
    }
    return Response.forbidden(body: {"msg": "This email is already exist"});
    // final query = Query<Profile>(context);
    // query.values = profile;

    // final addProfile = await query.insert();

    // return Response.ok(addProfile);
  }
}
