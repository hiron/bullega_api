import 'dart:math';

import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/login.dart';

class VarifyController extends ResourceController {
  VarifyController(this.context);
  ManagedContext context;

  @Operation.post()
  Future<Response> codeVarify() async {
    final Map<String, dynamic> body = await request.body.decode();

    final query = Query<Login>(context)
      ..where((l) => l.email).equalTo(body['email'] as String)
      ..where((l) => l.varifyCode).equalTo(body['varifyCode'] as int);

    final user = await query.fetchOne();

    if (user.id.isFinite) {
      query.values.varifyCode =
          [1, 2, 3, 4, 5].map((d) => Random().nextInt(9)).join("") as int;
      query.values.state = 'active';

      await query.updateOne();
      return Response.ok(user);
    }
    return Response.serverError();
  }
}
