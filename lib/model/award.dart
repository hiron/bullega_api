import '../bullega_api.dart';

class Award extends ManagedObject<_Award> implements _Award {
  @override
  void willInsert() {
    date = DateTime.now().toUtc();
  }
}

class _Award {
  static String tableName()=> 'award';
  @primaryKey
  int id;

  @Column(indexed: true, nullable: false)
  @Validate.length(lessThan: 100)
  String title;

  @Column(nullable: false)
  String body;

  @Column(nullable: true)
  String thumbnail;

  @Column(nullable: true)
  String image;

  @Column()
  int position;

  @Column()
  int points;

  DateTime date;
}
