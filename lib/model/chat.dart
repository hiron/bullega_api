import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/profile.dart';

class Chat extends ManagedObject implements _Chat {
  @override
  void willInsert() {
    // TODO: implement willInsert
    chatTime = DateTime.now().toUtc();
  }
}

class _Chat {
  static String tableName() => "chat";

  @primaryKey
  int id;

  @Column()
  String msg;

  @Relate(#chats, onDelete: DeleteRule.nullify, isRequired: true)
  Profile senderId;

  @Relate(#chats, onDelete: DeleteRule.nullify, isRequired: true)
  Profile receiverId;
  
  DateTime chatTime;
}
