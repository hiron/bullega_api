
import 'package:bullega_api/bullega_api.dart';
import 'package:bullega_api/model/profile.dart';
import 'package:password/password.dart';

class Login extends ManagedObject<_Login> implements _Login {
   @override
    void willInsert() {
    createDate = DateTime.now().toUtc();
    }

    @Serialize()
    void set password(String pw){
      hashedPassword = Password.hash(pw, PBKDF2());
    }
}

class _Login {
  static String tableName()=> 'login';
  @primaryKey
  int id;
  @Column(unique: true)
  @Validate.matches(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
  String email;

  // @Validate.length(greaterThanEqualTo: 5)
  @Column(omitByDefault: true)
  String hashedPassword;

  @Validate.oneOf(['active', 'lock'])
  @Column(defaultValue: 'lock')
  String state;

  // @Validate.length(equalTo:5)
  int varifyCode;

  DateTime createDate;

  @Relate(#login, onDelete: DeleteRule.cascade)
  Profile profile;
}
