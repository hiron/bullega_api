import '../bullega_api.dart';

class News extends ManagedObject<_News> implements _News {
  @override
  void willInsert() {
    date = DateTime.now().toUtc();
  }
}

class _News {
  static String tableName()=> 'news';
  @primaryKey
  int id;

  @Column(indexed: true, nullable: false)
  @Validate.length(lessThan: 100)
  String title;

  @Column(nullable: false)
  String body;

  @Column(nullable: true)
  String thumbnail;

  @Column(nullable: true)
  String image;

  @Column(indexed:true)
  DateTime date;
}
