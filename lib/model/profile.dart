import "package:bullega_api/bullega_api.dart";
import 'package:bullega_api/model/chat.dart';
import 'package:bullega_api/model/login.dart';

class Profile extends ManagedObject<_Profile> implements _Profile {}

class _Profile {
  static String tableName() => 'profile';
  @primaryKey
  int id;
  @Column()
  String firstname;

  String lastname;

  @Column()
  DateTime birthDate;

  @Validate.matches(
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$")
  @Column(unique: true)
  String email;

  @Column(nullable: true)
  String taxCode;

  @Column(nullable: true, unique: true)
  // @Validate.matches(r'^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$')
  String phone;

  @Column(defaultValue: 'no')
  @Validate.oneOf(['yes', 'no'])
  String motorcycle;

  @Column(defaultValue: 'no')
  @Validate.oneOf(['yes', 'no'])
  String paypal;

  @Column(nullable: true)
  String street;
  @Column(nullable: true)
  String region;
  @Column(nullable: true)
  String province;
  @Column(nullable: true)
  String city;

  @Column(nullable: true)
  String avatar;

  Login login;

  ManagedSet<Chat> chats;
}
